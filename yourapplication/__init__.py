__author__ = 'farly'

from flask import Flask


def create_app(env="production", params=None):

    if params is None:
        params = dict()

    app = Flask(
        __name__.split('.')[0],
        **params
    )

    # get config from environment
    app.config.from_object('config_{0}'.format(env))

    app.jinja_env.trim_blocks = True

    # Init extensions app
    extensions = [
        'db',
        'babel',
        'login_manager'
    ]

    ext = __import__("{0}.ext".format(__name__), fromlist=[extensions])

    for extension in extensions:
        getattr(ext, extension).init_app(app)

    # Register modules blueprint
    modules = [
        __name__,
        # or 'yourapplication.modules.core',
    ]

    for module in modules:
        blueprint = __import__(module + ".blueprint", fromlist=['blueprint'])
        app.register_blueprint(blueprint.blueprint)

    # Custom error handler
    setup_error_handler(app)

    # Setup logging handler
    setup_logging_handler(app)

    return app


def setup_error_handler(app):
    """Setup HTTP error handler.

    Use render_template if needs
    """

    @app.errorhandler(403)
    def handle_403(err):
        pass

    @app.errorhandler(404)
    def handle_404(err):
        pass

    @app.errorhandler(500)
    def handle_500(err):
        pass


def setup_logging_handler(app):
    """Setup logging handler

    In this example, using SMTPHandler
    """

    if not app.debug:
        import logging
        from logging.handlers import SMTPHandler
        mail_handler = SMTPHandler('smtp.yourdomain.com',
                                   'no-reply@yourodmain.com',
                                   app.config['ADMINS'],
                                   '[App] Application Failed!')
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)