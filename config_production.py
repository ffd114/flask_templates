__author__ = 'farly'

import os
basedir = os.path.abspath(os.path.dirname(__file__))

# Debug
DEBUG = False

# Set admins email
ADMINS = frozenset(['farly.fd@gmail.com'])

# Always generate new SECRET_KEY
SECRET_KEY = '4bf745d9-b66d-4762-968a-58c656c01192'

# Set SQLAlchemy Database
SQLALCHEMY_DATABASE_URI = 'sqllite://'

THREADS_PER_PAGE = 8