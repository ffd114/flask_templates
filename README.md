# Flask Templates

## Requirements

See `requirements.txt` or install it by using this command:
   
    $ pip install -r requirements.txt

## Preparing

1. Change *SECRET_KEY* inside *config_\*.py*, you can use *uuid.uuid4()*
to generate new key:

        import uuid
        print uuid.uuid4()

2. Set admin email in *config_\*.py*

# Large Applications

To create large application. You need to split each models, views, blueprint
into separate package for each application. You can follow this structure:

    yourapplication
    |
    +-- __init__.py
    +-- apps
       |
       +-- __init__.py
       +-- user
       |   |
       |   +-- __init__.py
       |   +-- blueprint.py
       |   +-- models.py
       |   +-- views.py
       +-- anothermodules
           |
           +-- __init__.py
           +-- blueprint.py
           +-- models.py
           +-- views.py
           
But I prefer to use `modules` instead of `apps`

Register blueprint at `yourapplication/__init__.py` line 35 according to each 
`apps` (see example)

# Requirements

## Database ORM

Yes, out there, there are lots of arguments whether to use ORM or not. But I 
choose to use it as it really makes my life easier.

It's good idea to write wrapper for every models you create. For example (using
SQLAlchemy): 

Model class:

    :::python
    from .ext import db
    
    class Student(db.Model):
        
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(255))
        
Wrapper class:

    :::python
    from .models import Student
    
    class StudentModel(object):
    
        @staticmethod
        def get_by_id(id):
            return Student.query.get_or_404(id=id)
            
        @staticmethod
        def get_by_name(name):
            return Student.query.get_or_404(name=name)
    
View:

    :::python
    
    # import something here
    ...
    
    def view_student(id=id):
        student = Student.get_by_id(id)
        
        # Do something here
        ...


This will be usefull when you want to change your ORM library in the future. 
Please note that I still not yet decided what is the package name for the 
wrappers should we use. I'm considering between `orms` for defining ORM and 
`models` for the wrapper

Of course, this means more work to do.

###  SQLAlchemy

According to its website [http://www.sqlalchemy.org/]():

> SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives
application developers the full power and flexibility of SQL.

#### Install :

* pip

        pip install flask-sqlalchemy
        
#### Documentation

* SQLAlchemy : [http://docs.sqlalchemy.org/](http://docs.sqlalchemy.org/)
* Flask-SQLAlchemy : [http://pythonhosted.org/Flask-SQLAlchemy/]
(http://pythonhosted.org/Flask-SQLAlchemy/)

## Unit Testing

## Tests

### Testing

#### Install :

* pip
    
        $ pip install flask-testing

#### Documentation
* Flask-Testing : [http://pythonhosted.org/Flask-Testing/](http://pythonhosted.org/Flask-Testing/)


## Running

To run server, we use `manage.py`

    $ python manage.py runserver