__author__ = 'farly'

import os
basedir = os.path.abspath(os.path.dirname(__file__))

# Debug
DEBUG = True

# Set admins email
ADMINS = frozenset(['farly.fd@gmail.com'])

# Always generate new SECRET_KEY
SECRET_KEY = '1e918659-b394-48df-be27-54b5c09335a7'

# Set SQLAlchemy Database
SQLALCHEMY_DATABASE_URI = 'sqllite://'

THREADS_PER_PAGE = 8